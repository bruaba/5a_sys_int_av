package jmetal.bruaba.TP2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Lecteur {
	private String nomFichier;

	public Lecteur(String nomFichier) {
		this.nomFichier = nomFichier;
	}

	public void lecture() {

		String ligne;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(nomFichier)));
			try {

				ligne = br.readLine();

				while (ligne != null) {
					ligne = br.readLine();
				}

				br.close();

			} catch (IOException e) {
				System.out.println("Erreur lors de la lecture : " + e.getMessage());
			}
		} catch (FileNotFoundException exc) {
			System.out.println("Erreur d'ouverture");
		}

	}

}
