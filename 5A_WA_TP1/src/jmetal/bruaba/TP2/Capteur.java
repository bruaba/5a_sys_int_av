package jmetal.bruaba.TP2;

public class Capteur {

	private Point position;
	private int rayonCouverture;

	public Capteur(int x, int y, int rayonCouverture) {
		this.setPosition(new Point(x, y));
		this.rayonCouverture = rayonCouverture;
	}

	public int getRayonCouverture() {
		return rayonCouverture;
	}

	public Point getPosition() {
		return position;
	}

	public void setRayonCouverture(int rayonCouverture) {
		this.rayonCouverture = rayonCouverture;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

}
