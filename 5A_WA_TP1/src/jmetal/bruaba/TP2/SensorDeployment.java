package jmetal.bruaba.TP2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;

public class SensorDeployment extends Problem {

	private Perimetre terrain;
	private ArrayList<Capteur> chromosome;
	private ArrayList<Point> cible;
	private int nbCapteur;
	private int rayonCouverture;
	private String nomFichier;
	private String separateur;

	public SensorDeployment(String solutionType, Integer numberOfVariables, int xa, int ya, int xb, int yb,
			int rayonCouverture, String nomFichier, String separateur) throws ClassNotFoundException {

		this.nomFichier = nomFichier;
		this.separateur = separateur;
		this.rayonCouverture = rayonCouverture;
		this.cible = new ArrayList<Point>();

		lecture();

		numberOfVariables_ = numberOfVariables;
		numberOfObjectives_ = 2;
		numberOfConstraints_ = 0;
		problemName_ = "SensorDeployment";

		if (solutionType.compareTo("Real") == 0) {
			solutionType_ = new RealSolutionType(this);
		} else {
			System.out.println("Error: solution type " + solutionType + " invalid");
			System.exit(-1);
		}

		lowerLimit_ = new double[numberOfVariables_];
		upperLimit_ = new double[numberOfVariables_];

		// definissez les bornes pour lowerLimit_ et upperLimit_ pour chaque variable de
		// d�cision
		for (int i = 0; i < numberOfVariables_; i++) {
			if (i % 2 == 0) {
				lowerLimit_[i] = xb;
				upperLimit_[i] = xa;
			} else {
				lowerLimit_[i] = yb;
				upperLimit_[i] = ya;
			}

		}

	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		// TODO Auto-generated method stub
		Variable[] decisionVariables = solution.getDecisionVariables();

		double[] x = new double[numberOfVariables_];

		for (int i = 0; i < numberOfVariables_; i++) {
			x[i] = decisionVariables[i].getValue();
		}

		double[] tableauCible = new double[cible.size()];
		for (int i = 0; i < tableauCible.length; i++) {
			tableauCible[i] = 0.0;
		}
		// maxCibleCouverte
		for (int i = 0; i < numberOfVariables_; i += 2) {
			for (int j = 0; j < cible.size(); j++) {

				if ((Math.pow((cible.get(j).getX() - x[i]), 2) + Math.pow((cible.get(j).getY() - x[i + 1]), 2)) < Math
						.pow(this.rayonCouverture, 2)) {
					tableauCible[j] += 1;
				}
			}
		}

		// cellule min different de zero
		double celluleMin = 0.0;
		for (int i = 0; i < tableauCible.length; i++) {
			if (tableauCible[i] != 0) {
				celluleMin += 1;
			}
		}

		// min du tableau
		double min = tableauCible[0];
		for (int i = 1; i < tableauCible.length; i++) {
			if (min > tableauCible[i]) {
				min = tableauCible[i];
			}
		}

		double f1 = -celluleMin;
		double f2 = -min;

		solution.setObjective(0, f1);
		solution.setObjective(1, f2);
	}

	public void lecture() {

		String ligne;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(this.nomFichier)));
			try {

				while ((ligne = br.readLine()) != null) {
					String cible[] = ligne.split(this.separateur);
					this.cible.add(new Point(Integer.parseInt(cible[0]), Integer.parseInt(cible[1])));
				}

				br.close();

			} catch (IOException e) {
				System.out.println("Erreur lors de la lecture : " + e.getMessage());
			}
		} catch (FileNotFoundException exc) {
			System.out.println("Erreur d'ouverture");
		}

	}

}
