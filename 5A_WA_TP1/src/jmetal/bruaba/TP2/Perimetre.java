package jmetal.bruaba.TP2;

public class Perimetre {

	private Point pointA;
	private Point pointB;

	public Perimetre(int xa, int ya, int xb, int yb) {
		this.pointA = new Point(xa, ya);
		this.pointB = new Point(xb, yb);
	}

	public int perimetre() {
		return 2 * (this.largeur() + this.longueur());
	}

	public int largeur() {
		return Math.abs(this.getPointA().getY() - this.getPointB().getY());
	}

	public int longueur() {
		return Math.abs(this.getPointA().getX() - this.getPointB().getX());
	}

	public Point getPointA() {
		return pointA;
	}

	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}

	public Point getPointB() {
		return pointB;
	}

	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}

}
