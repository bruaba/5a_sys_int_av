# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 15:11:13 2020

@author: cheikh
"""

''' Trains a simple convnet on the MNIST dataset 
'''

#from __future__ import print_function

import keras

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras import backend as K

batch_size = 128
num_classes = 10
epochs = 12

img_rows, img_cols = 28, 28

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

if K.image_data_format() == 'channels_first':
	x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
	x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
	input_shape = (1, img_rows, img_cols)
else:
	x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
	x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
	input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

x_train /= 255
x_test /= 255

print(' x train shape',x_train.shape)
print(' x test shape',x_test.shape)


y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

#model.add permet d'ajouter une couche au modèle
model = Sequential()

#on aura 32 feature map en sortie
#puisque c'est la 1er couche , on doit lui spécifier la taille d'où input_shape
model.add(Conv2D(32, kernel_size = (3,3), activation='relu', input_shape = input_shape))
#deuxieme couche avec 64 feature map de taille 3 par 3
model.add(Conv2D(64, (3,3), activation='relu'))
#on reduit la dimension on passe à 2 par 2
model.add(MaxPooling2D(pool_size = (2,2)))
#le dropout est une fonction qui permet de désactiver aléatoirement les sortie
#ainsi ça oblige le réseau à trouver des sorties différents
#améliore la généralisation du reseau (25% des cellules seront désactivées)
model.add(Dropout(0.25))
#une couche de Flatten qui prend toute les données pour en faire 1 seul vecteur => on a 1 seul dimension
model.add(Flatten())
#128 c'est le nombre de sortie de la couche Dense
#la couche dense fait la somme pondéré de toute les entrée et les passe à une fonction d'activation
#elle fait le MLP en grossomodo
#model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
#softmax est un standard 
#elle permet d'avoir l'activation qui va bien quand on fait une classsifiaction

model.add(Dense(num_classes, activation='softmax'))

#loss c'est la fonction de coût qu'on va utiliser
model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adadelta(), metrics=['accuracy'])

model.summary()

#fonction qui fait l'entrainement
model.fit(x_train, y_train, batch_size = batch_size, epochs = epochs, verbose = 1, validation_data=(x_test, y_test))

#evaluate toward test dataset_
score = model.evaluate(x_test, y_test, verbose=0)

print('Test loss:', score[0])
print('Test accuracy:', score[1])

model.save('Test_MNIST_CNN.h5')